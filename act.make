; ----------------
; Generated makefile from http://drushmake.me
; Permanent URL: http://drushmake.me/file.php?token=cf7a1730fd7c
; ----------------
;
; This is a working makefile - try it! Any line starting with a `;` is a comment.
  
; Core version
; ------------
; Each makefile should begin by declaring the core version of Drupal that all
; projects should be compatible with.
  
core = 7.x
  
; API version
; ------------
; Every makefile needs to declare its Drush Make API version. This version of
; drush make uses API version `2`.
  
api = 2
  
; Core project
; ------------
; In order for your makefile to generate a full Drupal site, you must include
; a core project. This is usually Drupal core, but you can also specify
; alternative core projects like Pressflow. Note that makefiles included with
; install profiles *should not* include a core project.
  
; Drupal 7.x. Requires the `core` property to be set to 7.x.
projects[drupal][version] = 7.x

  
  
; Modules
; --------
projects[admin_menu][version] = 3.0-rc4
projects[admin_menu][type] = "module"
projects[module_filter][version] = 2.0-alpha2
projects[module_filter][type] = "module"
projects[calendar][version] = 3.4
projects[special_menu_items][version] = 2.0
projects[special_menu_items][type] = "module"
projects[calendar][type] = "module"
projects[date][version] = 2.7
projects[date][type] = "module"
projects[email][version] = 1.2
projects[email][type] = "module"
projects[link][version] = 1.2
projects[link][type] = "module"
projects[imce][version] = 1.8
projects[imce][type] = "module"
projects[auto_nodetitle][version] = 1.0
projects[auto_nodetitle][type] = "module"
projects[captcha][version] = 1.0
projects[captcha][type] = "module"
projects[field_group][version] = 1.3
projects[field_group][type] = "module"
projects[gmap][version] = 2.8
projects[gmap][type] = "module"
projects[jquery_ui][version] = 1.x-dev
projects[jquery_ui][type] = "module"
projects[libraries][version] = 2.1
projects[libraries][type] = "module"
projects[token][version] = 1.5
projects[token][type] = "module"
projects[page_title][version] = 2.7
projects[page_title][type] = "module"
projects[recaptcha][version] = 1.10
projects[recaptcha][type] = "module"
projects[ckeditor][version] = 1.16
projects[ckeditor][type] = "module"
projects[jquery_update][version] = 2.3
projects[jquery_update][type] = "module"
projects[views][version] = 3.7
projects[views][type] = "module"
projects[webform][version] = 3.19
projects[webform][type] = "module"
projects[ctools][version] = 1.5
projects[ctools][type] = "module"
projects[drd_server][version] = 2.5
projects[drd_server][type] = "module"
projects[aes][version] = 1.8
projects[aes][type] = "module"
projects[views_bulk_operations][version] = 3.2
projects[views_bulk_operations][type] = "module"
projects[adminimal_admin_menu][version] = 1.5
projects[adminimal_admin_menu][type]= "module"
projects[entity][version] = 1.5
projects[entity][type] = "module"
projects[location][version] = 3.3
projects[location][type] = "module"
projects[image_captcha_refresh][version] = 1.5
projects[image_captcha_refresh][type] = "module"
projects[libraries][version] = 2.2
projects[libraries][type] = "module"
projects[panels][version] = 3.5
projects[panels][type] = "module"
projects[nice_menus][version] = 2.5
projects[nice_menus][type] = "module"
projects[responsive_menus][version] = 1.5
projects[responsive_menus][type] = "module"
projects[token_insert][version] = 2.4
projects[token_insert][type] = "module"




;---SEO Modues
projects[globalredirect][version] = 1.5
projects[globalredirect][type] = "module"
projects[google_analytics][version] = 1.4
projects[google_analytics][type] = "module"
projects[pathauto][version] = 1.2
projects[pathauto][type] = "module"
projects[xmlsitemap][version] = 2.0-rc2
projects[xmlsitemap][type] = "module"
projects[metatag][version] = 1.0-rc2
projects[metatag][type] = "module"

;projects[seotools][version] = 1.0-alpha6
;projects[seotools][type] = "module"
projects[redirect][version] = 1.0-rc1
projects[redirect][type] = "module"

; Custom Modules
; ---------------
;projects[act_testimonials][type] = "module"
;projects[act_testimonials][download][type] = "git"
;projects[act_testimonials][download][url]="git@bitbucket.org:act2281/act_testimonials.git"


;projects[act_contact_us][type] = "module"
;projects[act_contact_us][download][type] = "git"
;projects[act_contact_us][download][url]="git@bitbucket.org:act2281/contact-us.git"

;projects[act_user_setup][type] = "module"
;projects[act_user_setup][download][type] = "git"
;projects[act_user_setup][download][url]="git@bitbucket.org:act2281/user-setup.git"


;projects[act_services][type] = "module"
;projects[act_services][download][type] = "git"
;projects[act_services][download][url]="git@bitbucket.org:act2281/services.git"

;projects[act_products][type] = "module"
;projects[act_products][download][type] = "git"
;projects[act_products][download][url]= "git@bitbucket.org:act2281/product.git"

;projects[act_blog][type] = "module"
;projects[act_blog][download][type] = "git"
;projects[act_blog][download][url]= "git@bitbucket.org:act2281/act_blog.git"


; Themes
; --------
projects[zen][version] = 5.4
projects[zen][type] = "theme"
projects[adminimal_theme][type] = "theme"

  
  
; Libraries
; ---------
; No libraries were included
;libraries[ckeditor][download][type] = "get"
;libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.4.7/ckeditor_4.4.7_full.tar.gz"

